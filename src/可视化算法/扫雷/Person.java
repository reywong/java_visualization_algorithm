package 可视化算法.扫雷;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author MT
 * @date 2019-12-02 15:56
 */
public class Person {
    private String name;
    private int age;
    private boolean gender;
    private int id;

    public Person() {
    }

    public Person(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", id=" + id +
                '}';
    }

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>(3);
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            scanner = new Scanner(System.in);
            String personStr = scanner.nextLine();
            String[] s = personStr.split(" ");
            Person person = new Person(s[0], Integer.parseInt(s[1]), Boolean.parseBoolean(s[2]));
            personList.add(person);
        }
        scanner.close();
        personList.forEach(person ->
                System.out.println(person.toString()));
    }
}
