package 可视化算法.扫雷.等概率洗牌算法FisherYates;

/**
 * @author MT
 * @date 2019-12-02 17:14
 */
public class ShuffleExp {

    private int N;
    private int n, m;

    public ShuffleExp(int N, int n, int m) {
        if (N <= 0) {
            throw new IllegalArgumentException("N不能小于等于0");
        }
        if (n < m) {
            throw new IllegalArgumentException("n不能小于m");
        }
        this.N = N;
        this.n = n;
        this.m = m;
    }

    public void run() {
        int freq[] = new int[n];
        int arr[] = new int[n];
        for (int i = 0; i < N; i++) {
            // 排上雷
            reset(arr);
            // 洗牌
            shuffle(arr);
            for (int j = 0; j < n; j++) {
                freq[j] += arr[j];
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println(i + ":" + (double) freq[i] / N);
        }
    }

    private void shuffle(int[] arr) {
        for (int i = n - 1; i >= 0; i--) {
            int x = (int) (Math.random() * (i+1));
            swap(arr, i, x);

        }
    }

    private void swap(int[] arr, int i, int x) {
        int t = arr[i];
        arr[i] = arr[x];
        arr[x] = t;

    }

    private void reset(int[] arr) {
        for (int i = 0; i < m; i++) {
            arr[i] = 1;
        }
        for (int i = m; i < n; i++) {
            arr[i] = 0;
        }
    }

    public static void main(String[] args) {
        int N = 10000000;
        int n = 10;
        int m = 5;
        ShuffleExp shuffleExp = new ShuffleExp(N, n, m);
        shuffleExp.run();
    }
}
