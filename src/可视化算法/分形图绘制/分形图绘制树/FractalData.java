package 可视化算法.分形图绘制.分形图绘制树;

public class FractalData {

    public int depth;
    public double splitAngle;

    public FractalData(int depth, double splitAngle){
        this.depth = depth;
        this.splitAngle = splitAngle;
    }
}
