package 可视化算法.分形图绘制.Vicsek分形图绘制;

public class FractalData {

    private int depth;

    public FractalData(int depth){
        this.depth = depth;
    }

    public int getDepth(){
        return depth;
    }
}
