package 可视化算法.基础动画升级;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * @author MT
 * 工具类
 * @date 2019-11-28 15:15
 */
public class AlgoVisHelper {

    private AlgoVisHelper() {
    }

    public static final Color Red = new Color(0xF44336);
    public static final Color Pink = new Color(0xE91E63);
    public static final Color Purple = new Color(0x9C27B0);
    public static final Color DeepPurple = new Color(0x673AB7);
    public static final Color Indigo = new Color(0x3F51B5);
    public static final Color Blue = new Color(0x2196F3);
    public static final Color LightBlue = new Color(0x03A9F4);
    public static final Color Cyan = new Color(0x00BCD4);
    public static final Color Teal = new Color(0x009688);
    public static final Color Green = new Color(0x4CAF50);
    public static final Color LightGreen = new Color(0x8BC34A);
    public static final Color Lime = new Color(0xCDDC39);
    public static final Color Yellow = new Color(0xFFEB3B);
    public static final Color Amber = new Color(0xFFC107);
    public static final Color Orange = new Color(0xFF9800);
    public static final Color DeepOrange = new Color(0xFF5722);
    public static final Color Brown = new Color(0x795548);
    public static final Color Grey = new Color(0x9E9E9E);
    public static final Color BlueGrey = new Color(0x607D8B);
    public static final Color Black = new Color(0x000000);
    public static final Color White = new Color(0xFFFFFF);

    public static void setStrokeWidth(Graphics2D g2d, int w) {
        int strokeWidth = w;
        // 设置圆角与平滑拐弯
        g2d.setStroke(new BasicStroke(strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    }

    /**
     * 设置颜色
     *
     * @param g2d
     * @param color
     */
    public static void setColor(Graphics2D g2d, Color color) {
        g2d.setColor(color);
    }

    /**
     * x、y为圆心坐标
     *
     * @param g2d
     * @param x   长
     * @param y   宽
     * @param r   半径
     */
    public static void strokeCircle(Graphics2D g2d, int x, int y, int r) {
        // 绘制圆，设置距离左上角的距离与长宽
        Ellipse2D circle = new Ellipse2D.Double(x - r, y - r, 2 * r, 2 * r);
        g2d.draw(circle);
    }

    /**
     * x、y为圆心
     *
     * @param g2d
     * @param x   长
     * @param y   宽
     * @param r   半径
     */
    public static void fillCircle(Graphics2D g2d, int x, int y, int r) {
        // 绘制实心圆，设置距离左上角的距离与长宽
        Ellipse2D circle = new Ellipse2D.Double(x - r, y - r, 2 * r, 2 * r);
        g2d.fill(circle);
    }

    /**
     * 暂停t时间
     *
     * @param t
     */
    public static void pause(int t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void putImage(Graphics2D g2d, int x, int y, String imageURL) {
        ImageIcon imageIcon = new ImageIcon(imageURL);
        Image image = imageIcon.getImage();
        g2d.drawImage(image, x, y, null);
    }

    public static void drawText(Graphics2D g2d, String text, int centerx, int centery) {
        if (text == null) {
            throw new IllegalArgumentException("Text is null in drawText function!");
        }
        FontMetrics fontMetrics = g2d.getFontMetrics();
        int w = fontMetrics.stringWidth(text);
        int h = fontMetrics.getDescent();
        g2d.drawString(text, centerx - w / 2, centery + h);

    }

}
