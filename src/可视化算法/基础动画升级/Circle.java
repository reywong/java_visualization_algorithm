package 可视化算法.基础动画升级;

import java.awt.*;

/**
 * @author MT
 * <p>
 * 圆对象
 * @date 2019-11-28 17:54
 */
public class Circle {

    /**
     * 圆的编号
     */
    private int id;

    /**
     * 圆圈的圆心坐标
     */
    public int x, y;
    /**
     * 圆圈半径
     */
    private int r;

    /**
     * 圆的颜色
     */
    private Color color;

    /**
     * 速度
     */
    public int vx, vy;


    /**
     * 是否是实心圆
     */
    public boolean isFilled = false;

    /**
     * 构造函数
     */
    public Circle(int id, int x, int y, int r, int vx, int vy, Color color) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.r = r;
        this.vx = vx;
        this.vy = vy;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public int getR() {
        return r;
    }

    public Color getColor() {
        return color;
    }

    /**
     * 圆移动方法
     */
    public void move(int minx, int miny, int maxx, int maxy, Circle[] circles) {
        x += vx;
        y += vy;
        this.checkCollision(minx, miny, maxx, maxy);
        this.checkCircleCollision(circles);
    }

    /**
     * 圆与圆之间的碰撞
     *
     * @param circles
     */
    private void checkCircleCollision(Circle[] circles) {

        for (Circle circle : circles) {
            if (id == circle.getId()) {
                break;
            }
            int a = Math.abs(circle.x - x);
            int b = Math.abs(circle.y - y);
            double sqrt = Math.sqrt(a * a + b * b);
            if (sqrt <= circle.getR() + r) {
                // x,y位移距离
                int distance = (int) (Math.sqrt((2 * r - (int) sqrt) / 2));
                if (x > circle.x) {
                    x += distance;
                    circle.x -= distance;
                } else {
                    x -= distance;
                    circle.x += distance;
                }
                if (y > circle.y) {
                    y += distance;
                    circle.y -= distance;
                } else {
                    y -= distance;
                    circle.y += distance;
                }
                vy = -vy;
                vx = -vx;

                circle.vx = -vx;
                circle.vy = -vy;
            }
            System.out.println(String.format("x:%d,y:%d -> c.x:%d,c.y:%d", x, y, circle.x, circle.y));
        }

    }

    /**
     * 圆的碰撞窗口边缘检测
     *
     * @param minx x的最小值=窗口左边缘
     * @param miny y的最小值=窗口上边缘
     * @param maxx x的最大值=窗口右边缘
     * @param maxy y的最大值=窗口下边缘
     */
    private void checkCollision(int minx, int miny, int maxx, int maxy) {

        if (x - r < minx) {

            // 如果圆心坐标减去半径小于窗口左边缘
            // 那么圆心等于半径，速度为反方向运动
            x = r;
            vx = -vx;
        }
        if (x + r >= maxx) {

            // 如果圆心坐标加上半径小于窗口右边缘
            // 那么圆心等于半径，速度为反方向运动
            x = maxx - r;
            vx = -vx;
        }
        if (y - r < miny) {
            y = r;
            vy = -vy;
        }
        if (y + r >= maxy) {
            y = maxy - r;
            vy = -vy;
        }
    }


    /**
     * x平方 + y平方 <= r平方
     * 点到圆心的距离是否等于半径的平方
     *
     * @param p
     * @return
     */
    public boolean contain(Point p) {
        return (x - p.x) * (x - p.x) + (y - p.y) * (y - p.y) <= r * r;
    }
}
