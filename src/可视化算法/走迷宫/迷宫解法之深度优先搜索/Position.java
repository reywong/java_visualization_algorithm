package 可视化算法.走迷宫.迷宫解法之深度优先搜索;

public class Position {

    private int x, y;
    private Position prev;

    public Position(int x, int y, Position prev){
        this.x = x;
        this.y = y;
        this.prev = prev;
    }

    public Position(int x, int y){
        this(x, y, null);
    }

    public int getX(){return x;}
    public int getY(){return y;}
    public Position getPrev(){return prev;}
}
