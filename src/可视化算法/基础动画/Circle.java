package 可视化算法.基础动画;

import java.awt.*;

/**
 * @author MT
 * <p>
 * 圆对象
 * @date 2019-11-28 17:54
 */
public class Circle {

    /**
     * 圆圈的圆心坐标
     */
    public int x, y;
    /**
     * 圆圈半径
     */
    private int r;

    /**
     * 速度
     */
    public int vx, vy;

    /**
     * 是否是实心圆
     */
    public boolean isFilled = false;

    /**
     * 构造函数
     */
    public Circle(int x, int y, int r, int vx, int vy) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.vx = vx;
        this.vy = vy;
    }

    /**
     * 私有属性半径的获取方式
     *
     * @return
     */
    public int getR() {
        return r;
    }

    /**
     * 圆移动方法
     */
    public void move(int minx, int miny, int maxx, int maxy) {
        x += vx;
        y += vy;
        this.checkCollision(minx, miny, maxx, maxy);
    }

    /**
     * 圆的碰撞检测
     *
     * @param minx x的最小值=窗口左边缘
     * @param miny y的最小值=窗口上边缘
     * @param maxx x的最大值=窗口右边缘
     * @param maxy y的最大值=窗口下边缘
     */
    private void checkCollision(int minx, int miny, int maxx, int maxy) {

        if (x - r < minx) {
            x = r;
            vx = -vx;
        }
        if (x + r >= maxx) {
            x = maxx - r;
            vx = -vx;
        }
        if (y - r < miny) {
            y = r;
            vy = -vy;
        }
        if (y + r >= maxy) {
            y = maxy - r;
            vy = -vy;
        }
    }

    /**
     * x平方 + y平方 <= r平方
     * 点到圆心的距离是否等于半径的平方
     * @param p
     * @return
     */
    public boolean contain(Point p) {
        return (x - p.x) * (x - p.x) + (y - p.y) * (y - p.y) <= r * r;
    }
}
