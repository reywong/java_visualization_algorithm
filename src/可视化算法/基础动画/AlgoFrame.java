package 可视化算法.基础动画;

import 可视化算法.基础绘图.AlgoVisHelper;

import javax.swing.*;
import java.awt.*;

/**
 * @author MT
 * <p>
 * <p>
 * 视图层
 * @date 2019-11-28 14:30
 */
public class AlgoFrame extends JFrame {

    /**
     * 窗口宽
     */
    private int canvasWidth;

    /**
     * 窗口高
     */
    private int canvasHeight;

    /**
     * 构造方法
     *
     * @param title        标题
     * @param canvasWidth  宽
     * @param canvasHeight 高
     */
    public AlgoFrame(String title, int canvasWidth, int canvasHeight) {
        super(title);

        // 传入宽和高，只提供获取方法，不允许修改
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        // 创建画布
        AlgoCanvas canvas = new AlgoCanvas();
        // 设置画布大小;注释掉使用覆盖方法返回窗口大小
//        canvas.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
        // 设置窗口的内容面板
        this.setContentPane(canvas);
        this.pack();

        // 设置自己的默认窗口
        this.setSize(canvasWidth, canvasHeight);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * 构造方法
     *
     * @param tilte
     */
    public AlgoFrame(String tilte) {
        // 设置传入的标题和默认1024的宽和高
        this(tilte, 1024, 1024);
    }

    // TODO: 2019-11-28 新增的属性
    private Circle[] circles;
    // TODO: 2019-11-28 新增的方法
    public void render(Circle[] circles) {
        this.circles = circles;
        // JFrame提供的重新刷新AlgoCanvas的方法；清空AlgoCanvas，重新调用paintComponent()方法
        this.repaint();
    }

    /**
     * 私有内部类，只提供给AlgoFrame使用
     */
    private class AlgoCanvas extends JPanel {

        /**
         * 双缓存
         * JPanel默认支持
         * 只需要初始化时设置就行
         */
        public AlgoCanvas() {
            super(true);
        }

        /**
         * 绘制方法
         *
         * @param g 自动传入的参数
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            // Graphics画图方法
            // 创建距离左上角50，50，宽高300的圆
            // g.drawOval(50, 50, 300, 300);

            // Graphics2D画图方法
            Graphics2D g2d = (Graphics2D) g;

            // TODO: 2019-11-28 抗锯齿
            RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.addRenderingHints(hints);

            // TODO: 2019-11-29 具体的绘制
            // 设置笔画粗细
            AlgoVisHelper.setStrokeWidth(g2d, 1);
            // 设置颜色=红色
            AlgoVisHelper.setColor(g2d, Color.RED);
            for (Circle circle : circles) {
                if (!circle.isFilled){
                    // 如果是实心圆
                    AlgoVisHelper.strokeCircle(g2d, circle.x, circle.y, circle.getR());
                }else {
                    // 如果不是实心圆
                    AlgoVisHelper.fillCircle(g2d, circle.x, circle.y, circle.getR());
                }
            }
        }

        /**
         * 直接创建窗口大小的画布，上方手动创建已经被注释
         *
         * @return
         */
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }

    /**
     * CanvasWidth只生成get方法
     *
     * @return
     */
    public int getCanvasWidth() {
        return canvasWidth;
    }

    /**
     * CanvasHeight只生成get方法
     *
     * @return
     */
    public int getCanvasHeight() {
        return canvasHeight;
    }

    /**
     * 运行测试自定义的JFrame
     *
     * @param args
     */
    public static void main(String[] args) {
        AlgoFrame myJFrame = new AlgoFrame("myJFrame", 500, 500);
//        AlgoFrameByHelper myJFrame2 = new AlgoFrameByHelper("myJFrame,默认窗口");

    }
}
