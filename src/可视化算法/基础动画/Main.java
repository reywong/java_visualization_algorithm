package 可视化算法.基础动画;

import 可视化算法.基础绘图.AlgoVisHelper;

import java.awt.*;

/**
 * @author MT
 * 执行方法
 * @date 2019-11-28 14:15
 */
public class Main {

    public static void main(String[] args) {

        // 场景宽
        int sceneWidth = 900;
        // 场景高
        int sceneHeight = 900;
        // 生成圆的数量
        int N = 10;
        // 圆的半径
        int R = 50;
        // 创建N个圆
        Circle[] circles = new Circle[N];
        for (int i = 0; i < N; i++) {
            // 生成0~1之间的随机数x,y，当做圆心坐标
            int x = (int) (Math.random() * (sceneWidth - 2 * R)) + R;
            int y = (int) (Math.random() * (sceneHeight - 2 * R)) + R;

            // 生成0~5的随机数速度
            int vx = (int) (Math.random() * 11) - 3;
            int vy = (int) (Math.random() * 11) - 3;

            circles[i] = new Circle(x, y, R, vx, vy);
        }
        EventQueue.invokeLater(() -> {
            AlgoFrame frame = new AlgoFrame("基础动画之动态圆", sceneWidth, sceneHeight);

            // 必需使用新线程才能渲染无限运动的动画，不然会被GUI绘制队列阻塞
            new Thread(() -> {
                //循环成动画
                while (true) {
                    //绘制数据
                    frame.render(circles);
                    AlgoVisHelper.pause(50);
                    //更新数据
                    for (Circle circle : circles) {
                        circle.move(0,0,sceneWidth,sceneHeight);
                    }
                }
            }).start();
        });
    }
}
