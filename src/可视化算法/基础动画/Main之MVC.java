package 可视化算法.基础动画;

/**
 * @author MT
 * MVC执行方法
 * @date 2019-11-28 14:15
 */
public class Main之MVC {

    public static void main(String[] args) {
        int sceneWidth = 800;
        int sceneHeight = 800;
        int N = 10;
        AlgoVisualizer algoVisualizer = new AlgoVisualizer(sceneWidth, sceneHeight, N);
    }
}
