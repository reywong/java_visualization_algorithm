package 可视化算法.基础动画;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;

/**
 * @author MT
 * 本章教学生成的模板供后面章节学习可视化算法使用
 * @date 2019-11-29 11:25
 */
public class 最后提炼出模板 {

    /**
     * 数据
     */
    private Object data;

    /**
     * 视图
     */
    private AlgoFrame frame;


    /**
     * 构造函数
     * @param sceneWidth
     * @param sceneHeight
     * @param N
     */
    public 最后提炼出模板(int sceneWidth, int sceneHeight, int N) {

        // TODO: 2019-11-29 初始化数据

        EventQueue.invokeLater(() -> {
            this.frame = new AlgoFrame("基础动画之动态圆", sceneWidth, sceneHeight);
            // TODO: 2019-11-29 根据情况决定是否加入键盘鼠标事件监听器
            this.frame.addKeyListener(new AlgoKeyListener());
            this.frame.addMouseListener(new AlgoMouseListener());
            new Thread(() -> {
                this.run();
            }).start();
        });
    }

    // TODO: 2019-11-29 编写自己的动画逻辑
    private void run() {}

    // TODO: 2019-11-29 根据情况决定是否实现键盘鼠标等交互事件监听器类
    private class AlgoKeyListener extends KeyAdapter {}
    private class AlgoMouseListener extends MouseAdapter {}

    public static void main(String[] args) {
        int sceneWidth = 800;
        int sceneHeight = 800;
        int N = 10;
        最后提炼出模板 algoVisualizer = new 最后提炼出模板(sceneWidth, sceneHeight, N);
    }
}
