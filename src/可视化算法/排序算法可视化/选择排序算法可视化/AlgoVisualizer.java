package 可视化算法.排序算法可视化.选择排序算法可视化;

import java.awt.*;

public class AlgoVisualizer {

    private static int DELAY = 10;

    private SelectionSortData data;
    private AlgoFrame frame;

    public AlgoVisualizer(int sceneWidth, int sceneHeight, int N){

        // 初始化数据
        data = new SelectionSortData(N, sceneHeight);

        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("Selection Sort Visualization", sceneWidth, sceneHeight);
            new Thread(() -> {
                run();
            }).start();
        });
    }

    // 动画逻辑
    private void run(){

        frame.render(data);
        AlgoVisHelper.pause(DELAY);

        for( int i = 0 ; i < data.N() ; i ++ ){
            // 选择排序：寻找[i, n)区间里的最小值的索引
            int minIndex = i;
            for( int j = i + 1 ; j < data.N() ; j ++ ){

                frame.render(data);
                AlgoVisHelper.pause(DELAY);
                if( data.get(j) < data.get(minIndex) ){
                    minIndex = j;
                    frame.render(data);
                    AlgoVisHelper.pause(DELAY);
                }
            }

            data.swap(i , minIndex);
            frame.render(data);
            AlgoVisHelper.pause(DELAY);
        }

        frame.render(data);
        AlgoVisHelper.pause(DELAY);
    }

    public static void main(String[] args) {

        int sceneWidth = 800;
        int sceneHeight = 800;
        // 排序元素的数量
        int N = 100;

        AlgoVisualizer vis = new AlgoVisualizer(sceneWidth, sceneHeight, N);
    }
}