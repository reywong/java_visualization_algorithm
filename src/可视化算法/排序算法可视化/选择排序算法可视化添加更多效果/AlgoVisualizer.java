package 可视化算法.排序算法可视化.选择排序算法可视化添加更多效果;

import java.awt.*;

public class AlgoVisualizer {

    private static int DELAY = 5;

    private SelectionSortData data;
    private AlgoFrame frame;

    public AlgoVisualizer(int sceneWidth, int sceneHeight, int N) {

        // 初始化数据
        data = new SelectionSortData(N, sceneHeight);

        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("选择排序算法可视化", sceneWidth, sceneHeight);
            new Thread(() -> {
                this.run();
            }).start();
        });
    }

    // 动画逻辑
    private void run() {

        this.setData(0, -1, -1);

        for (int i = 0; i < data.N(); i++) {
            // 选择排序：寻找[i, n)区间里的最小值的索引
            int minIndex = i;
            this.setData(i, -1, minIndex);

            for (int j = i + 1; j < data.N(); j++) {
                this.setData(i, j, minIndex);
                if (data.get(j) < data.get(minIndex)) {
                    minIndex = j;
                    this.setData(i, j, minIndex);
                }
            }

            data.swap(i, minIndex);
            this.setData(i + 1, -1, -1);
        }

        this.setData(data.N(), -1, -1);
    }

    private void setData(int orderedIndex, int currentCompareIndex, int currentMinIndex) {
        data.orderedIndex = orderedIndex;
        data.currentCompareIndex = currentCompareIndex;
        data.currentMinIndex = currentMinIndex;

        frame.render(data);
        AlgoVisHelper.pause(DELAY);
    }

    public static void main(String[] args) {

        int sceneWidth = 800;
        int sceneHeight = 800;
        // 排序元素的数量
        int N = 100;

        AlgoVisualizer vis = new AlgoVisualizer(sceneWidth, sceneHeight, N);
    }
}