package 可视化算法.排序算法可视化.插入排序可视化之近乎有序的数;

import java.util.Arrays;

/**
 * @author MT
 * 排序数对象
 * @date 2019-11-29 11:25
 */
public class InsertionSortData {

    public enum Type {
        Default,
        NearlyOrdered

    }    // 排序数组

    private int[] numbers;
    // [ 0 .... orderedIndex) 是有序的
    public int orderedIndex = -1;
    public int currentIndex = -1;

    public InsertionSortData(int N, int randomBound, Type dataType) {

        numbers = new int[N];

        for (int i = 0; i < N; i++)
            numbers[i] = (int) (Math.random() * randomBound) + 1;

        if (dataType == Type.NearlyOrdered) {
            Arrays.sort(numbers);
            int swapTime = (int) (0.02 * N);
            for (int i = 0; i < swapTime; i++) {
                int a = (int) (Math.random() * N);
                int b = (int) (Math.random() * N);
                swap(a, b);
            }
        }
    }

    public InsertionSortData(int N,int randomBound){
        this(N,randomBound,Type.Default);
    }

    public int N() {
        return numbers.length;
    }

    public int get(int index) {
        if (index < 0 || index >= numbers.length) {
            throw new IllegalArgumentException("Invalid index to access Sort Data.");
        }

        return numbers[index];
    }

    /**
     * 交换元素位置
     *
     * @param i
     * @param j
     */
    public void swap(int i, int j) {

        if (i < 0 || i >= numbers.length || j < 0 || j >= numbers.length) {
            throw new IllegalArgumentException("Invalid index to access Sort Data.");
        }

        int t = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = t;
    }
}
