package 可视化算法.排序算法可视化.归并排序可视化;

/**
 * @author MT
 * 排序数对象
 * @date 2019-11-29 11:25
 */
public class MergeSortData {

    public int[] numbers;
    // 左右边界索引
    public int l,r;
    public int mergeIndex;

    public MergeSortData(int N, int randomBound) {

        numbers = new int[N];

        for (int i = 0; i < N; i++)
            numbers[i] = (int) (Math.random() * randomBound) + 1;

    }

    public int N() {
        return numbers.length;
    }

    public int get(int index) {
        if (index < 0 || index >= numbers.length) {
            throw new IllegalArgumentException("Invalid index to access Sort Data.");
        }

        return numbers[index];
    }

    /**
     * 交换元素位置
     *
     * @param i
     * @param j
     */
    public void swap(int i, int j) {

        if (i < 0 || i >= numbers.length || j < 0 || j >= numbers.length) {
            throw new IllegalArgumentException("Invalid index to access Sort Data.");
        }

        int t = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = t;
    }
}
