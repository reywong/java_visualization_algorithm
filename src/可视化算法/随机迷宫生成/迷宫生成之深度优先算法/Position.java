package 可视化算法.随机迷宫生成.迷宫生成之深度优先算法;

public class Position {

    private int x, y;

    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){return x;}
    public int getY(){return y;}
}
