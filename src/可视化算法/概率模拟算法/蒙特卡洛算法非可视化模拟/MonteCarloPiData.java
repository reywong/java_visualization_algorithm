package 可视化算法.概率模拟算法.蒙特卡洛算法非可视化模拟;


import java.awt.*;
import java.util.LinkedList;

/**
 * @author MT
 * @date 2019-11-29 13:45
 */
public class MonteCarloPiData {


    private Circle circle;
    private int insideCircle = 0;
    private LinkedList<Point> points;

    public MonteCarloPiData(Circle circle) {
        this.circle = circle;
        points = new LinkedList<>();
    }


    public Circle getCircle() {
        return circle;
    }

    public Point getPoints(int i) {
        if (i < 0 || i >= points.size()) {
            throw new IllegalArgumentException("out of bound in getPoint");
        }
        return points.get(i);
    }

    public int getPointsNumber() {
        return points.size();
    }

    public void addPoint(Point point) {
        points.add(point);
        if (circle.contain(point)) {
            insideCircle++;
        }
    }

    public double estimatePi() {

        if (points.size() == 0) {
            return 0.0;
        }

        int circleArea = insideCircle;
        int squareArea = points.size();
        return (double) circleArea * 4 / squareArea;
    }

}
