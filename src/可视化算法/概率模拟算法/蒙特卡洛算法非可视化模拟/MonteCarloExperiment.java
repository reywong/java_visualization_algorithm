package 可视化算法.概率模拟算法.蒙特卡洛算法非可视化模拟;

import java.awt.*;

/**
 * @author MT
 * @date 2019-11-29 14:03
 */
public class MonteCarloExperiment {

    // 每多少个输出一次
    private int outputInterval = 100;
    // 长方形的款
    private int squareSide;
    // 点的个数
    private int N;

    public MonteCarloExperiment(int squareSide, int N) {
        if (squareSide <= 0 || N <= 0) {
            throw new IllegalArgumentException("值必须大于零");
        }
        this.squareSide = squareSide;
        this.N = N;
    }
    public void setOutputInterval(int interval){
        if (interval < 0){
            throw new IllegalArgumentException("interval不能小于零");
        }
        this.outputInterval = interval;
    }

    private void run() {
        Circle circle = new Circle(squareSide / 2, squareSide / 2, squareSide / 2);
        MonteCarloPiData data = new MonteCarloPiData(circle);
        for (int i = 0; i < N; i++) {
            if (i % outputInterval == 0) {
                System.out.println(data.estimatePi());
            }
            int x = (int) (Math.random() * squareSide);
            int y = (int) (Math.random() * squareSide);
            data.addPoint(new Point(x, y));
        }
    }


    public static void main(String[] args) {

        int squareSide = 800;
        int N = 1000;

        MonteCarloExperiment exp = new MonteCarloExperiment(squareSide, N);
        exp.setOutputInterval(10);
        exp.run();
    }
}
