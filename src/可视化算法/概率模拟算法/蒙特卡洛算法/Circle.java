package 可视化算法.概率模拟算法.蒙特卡洛算法;

import java.awt.*;

/**
 * @author MT
 * 圆对象
 * @date 2019-11-28 17:54
 */
public class Circle {

    private int x, y, r;


    /**
     * 构造函数
     */
    public Circle(int x, int y, int r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getR() {
        return r;
    }

    /**
     * x平方 + y平方 <= r平方
     * 点到圆心的距离是否等于半径的平方
     *
     * @param p
     * @return
     */
    public boolean contain(Point p) {
        return Math.pow(p.getX() - x, 2) + Math.pow(p.getY() - y, 2) <= r * r;
    }
}
