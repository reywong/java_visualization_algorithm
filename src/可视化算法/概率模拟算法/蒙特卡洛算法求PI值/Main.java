package 可视化算法.概率模拟算法.蒙特卡洛算法求PI值;


/**
 * @author MT
 * MVC执行方法
 * @date 2019-11-28 14:15
 */
public class Main {

    public static void main(String[] args) {
        int sceneWidth = 800;
        int sceneHeight = 800;
        int N = 100000;
        AlgoVisualizer algoVisualizer = new AlgoVisualizer(sceneWidth, sceneHeight,N);
    }
}
