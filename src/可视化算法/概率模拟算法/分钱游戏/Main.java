package 可视化算法.概率模拟算法.分钱游戏;


/**
 * @author MT
 * MVC执行方法
 * @date 2019-11-28 14:15
 */
public class Main {

    public static void main(String[] args) {
        int sceneWidth = 1000;
        int sceneHeight = 800;
        AlgoVisualizer algoVisualizer = new AlgoVisualizer(sceneWidth, sceneHeight);
    }
}
