package 可视化算法.概率模拟算法.三门问题;

/**
 * @author MT
 * 你会不会中奖？
 * @date 2019-11-29 14:55
 */
public class WinningPrize {

    // 设置中奖概率
    private double chance;
    // 设置开奖次数
    private int playTime;
    // 设置实验次数
    private int N;

    public WinningPrize(double chance, int playTime, int n) {
        if (chance < 0.0 || chance > 1.0) {
            throw new IllegalArgumentException("chance 不能小于零");
        }
        if (playTime <= 0 || n <= 0) {
            throw new IllegalArgumentException("playTime/n 不能小于零");
        }
        this.chance = chance;
        this.playTime = playTime;
        N = n;
    }

    public void run(){
        int wins = 0;
        for (int i = 0; i < N; i++) {
            if (play()){
                wins++;
            }

        }
        System.out.println("winning rate:" + (double)wins/N);
    }
    private boolean play(){
        for (int i = 0; i < playTime; i++) {
            if (Math.random() < chance){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        // 设置中奖概率
        double chance = 0.2;
        // 设置开奖次数
        int playTime = 5;
        // 设置实验次数
        int N = 1000000;
        WinningPrize winningPrize = new WinningPrize(chance, playTime, N);
        winningPrize.run();
    }
}
