package 可视化算法.基础绘图;

import javax.swing.*;
import java.awt.*;

/**
 * @author MT
 * <p>
 * // 第二个类：创建属于自己的JFrame,自定义JFrame
 * // 第三个类：修改第二个类通过工具类简化
 * 第四个类：在第三个类上增加抗锯齿
 * 使用Graphics
 * @date 2019-11-28 14:30
 */
public class AlgoFrameByHelper抗锯齿 extends JFrame {

    /**
     * 窗口宽
     */
    private int canvasWidth;

    /**
     * 窗口高
     */
    private int canvasHeight;

    /**
     * 构造方法
     *
     * @param title        标题
     * @param canvasWidth  宽
     * @param canvasHeight 高
     */
    public AlgoFrameByHelper抗锯齿(String title, int canvasWidth, int canvasHeight) {
        super(title);

        // 传入宽和高，只提供获取方法，不允许修改
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        // 创建画布
        AlgoCanvas canvas = new AlgoCanvas();
        // 设置画布大小;注释掉使用覆盖方法返回窗口大小
//        canvas.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
        // 设置窗口的内容面板
        this.setContentPane(canvas);
        this.pack();

        // 设置自己的默认窗口
        this.setSize(canvasWidth, canvasHeight);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * 构造方法
     *
     * @param tilte
     */
    public AlgoFrameByHelper抗锯齿(String tilte) {
        // 设置传入的标题和默认1024的宽和高
        this(tilte, 1024, 1024);
    }

    /**
     * 私有内部类，只提供给AlgoFrame使用
     */
    private class AlgoCanvas extends JPanel {

        /**
         * 绘制方法
         *
         * @param g 自动传入的参数
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            // Graphics画图方法
            // 创建距离左上角50，50，宽高300的圆
            // g.drawOval(50, 50, 300, 300);

            // Graphics2D画图方法
            Graphics2D g2d = (Graphics2D) g;

            // TODO: 2019-11-28 抗锯齿
            RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.addRenderingHints(hints);

            // 设置笔画宽度
            AlgoVisHelper.setStrokeWidth(g2d,5);
            // 设置圆线条颜色
            AlgoVisHelper.setColor(g2d,Color.BLUE);
            AlgoVisHelper.strokeCircle(g2d,canvasWidth/2,canvasHeight/2,200);
            AlgoVisHelper.setColor(g2d,Color.RED);
            AlgoVisHelper.fillCircle(g2d,canvasWidth/2,canvasHeight/2,200);
        }

        /**
         * 直接创建窗口大小的画布，上方手动创建已经被注释
         *
         * @return
         */
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }

    /**
     * CanvasWidth只生成get方法
     *
     * @return
     */
    public int getCanvasWidth() {
        return canvasWidth;
    }

    /**
     * CanvasHeight只生成get方法
     *
     * @return
     */
    public int getCanvasHeight() {
        return canvasHeight;
    }

    /**
     * 运行测试自定义的JFrame
     *
     * @param args
     */
    public static void main(String[] args) {
        AlgoFrameByHelper抗锯齿 myJFrame = new AlgoFrameByHelper抗锯齿("myJFrame", 500, 500);
//        AlgoFrameByHelper myJFrame2 = new AlgoFrameByHelper("myJFrame,默认窗口");

    }
}
