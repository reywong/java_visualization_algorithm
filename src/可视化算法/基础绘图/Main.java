package 可视化算法.基础绘图;

import javax.swing.*;
import java.awt.*;

/**
 * @author MT
 *
 * 第一个类：学习创建窗口
 *
 * 为什么要用可视化算法？
 * 1. 为了使学习更加有趣
 * 2. 为了看到程序的运行过程，更加方便的了解算法的运行过程
 *
 * @date 2019-11-28 14:15
 */
public class Main {

    /**
     * 学习使用swing化窗口界面
     */
    public static void main(String[] args) {

        // 官方推荐方法创建窗口,据说更加安全；使用lambda表达式传入窗口
        EventQueue.invokeLater(() -> {

            JFrame jFrame = new JFrame("可视化算法：HelloWorld");
            // 设置窗口大小
            jFrame.setSize(500,500);
            // 设置窗口大小无法改变
            jFrame.setResizable(false);
            // 设置显示"关闭"按钮
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            // 设置窗口是否可见
            jFrame.setVisible(true);
        });
    }
}
