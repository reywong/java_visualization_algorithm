package 可视化算法.基础绘图;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * @author MT
 * <p>
 * 第二个类：创建属于自己的JFrame,自定义JFrame
 * 使用Graphics
 * @date 2019-11-28 14:30
 */
public class AlgoFrame extends JFrame {

    /**
     * 窗口宽
     */
    private int canvasWidth;

    /**
     * 窗口高
     */
    private int canvasHeight;

    /**
     * 构造方法
     *
     * @param title        标题
     * @param canvasWidth  宽
     * @param canvasHeight 高
     */
    public AlgoFrame(String title, int canvasWidth, int canvasHeight) {
        super(title);

        // 传入宽和高，只提供获取方法，不允许修改
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        // 创建画布
        AlgoCanvas canvas = new AlgoCanvas();
        // 设置画布大小;注释掉使用覆盖方法返回窗口大小
//        canvas.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
        // 设置窗口的内容面板
        this.setContentPane(canvas);
        this.pack();

        // 设置自己的默认窗口
        this.setSize(canvasWidth, canvasHeight);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * 构造方法
     *
     * @param tilte
     */
    public AlgoFrame(String tilte) {
        // 设置传入的标题和默认1024的宽和高
        this(tilte, 1024, 1024);
    }

    /**
     * 私有内部类，只提供给AlgoFrame使用
     */
    private class AlgoCanvas extends JPanel {

        /**
         * 绘制方法
         *
         * @param g 自动传入的参数
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            // Graphics画图方法
            // 创建距离左上角50，50，宽高300的圆
            // g.drawOval(50, 50, 300, 300);

            // Graphics2D画图方法
            Graphics2D g2d = (Graphics2D) g;

            // 设置笔画宽度
            g2d.setStroke(new BasicStroke(10));

            // 设置圆线条颜色
            g2d.setColor(Color.BLUE);
            Ellipse2D circle = new Ellipse2D.Double(50, 50, 300, 300);
            g2d.draw(circle);

            // 绘制实心圆；基于状态的图形绘制，将生产BLUE颜色的实心圆，要想设置实心圆颜色，需要重新设置g2d.setColor(Color.RED);
            g2d.setColor(Color.RED);
            Ellipse2D circle2 = new Ellipse2D.Double(60,60,280,280);
            g2d.fill(circle2);
        }

        /**
         * 直接创建窗口大小的画布，上方手动创建已经被注释
         *
         * @return
         */
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }

    /**
     * CanvasWidth只生成get方法
     *
     * @return
     */
    public int getCanvasWidth() {
        return canvasWidth;
    }

    /**
     * CanvasHeight只生成get方法
     *
     * @return
     */
    public int getCanvasHeight() {
        return canvasHeight;
    }

    /**
     * 运行测试自定义的JFrame
     *
     * @param args
     */
    public static void main(String[] args) {
//        AlgoFrame myJFrame = new AlgoFrame("myJFrame", 500, 500);
        AlgoFrame myJFrame2 = new AlgoFrame("myJFrame,默认窗口");

    }
}
