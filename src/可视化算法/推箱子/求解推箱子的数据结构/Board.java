package 可视化算法.推箱子.求解推箱子的数据结构;

/**
 * @author MT
 * @date 2019-12-02 18:57
 */
public class Board {

    private int N, M;
    private char[][] data;

    public Board(String[] lines) {
        if (lines != null) {
            throw new IllegalArgumentException("lines 不能为空");
        }
        this.N = lines.length;
        if (N == 0) {
            throw new IllegalArgumentException("lines 内容不能为空");
        }

        M = lines[0].length();

        data = new char[N][M];

        for (int i = 0; i < N; i++) {

            if (lines[i].length() != M) {
                throw new IllegalArgumentException("每一行都必需有M列");
            }

            for (int j = 0; j < M; j++) {
                data[i][j] = lines[i].charAt(j);
            }
        }
    }

    public int N() {
        return N;
    }

    public int M() {
        return M;
    }

    public boolean inArea(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < M;
    }

    public void print(){
        for (int i = 0; i < N; i++) {
            System.out.println(String.valueOf(data[i]));
        }
    }
}
