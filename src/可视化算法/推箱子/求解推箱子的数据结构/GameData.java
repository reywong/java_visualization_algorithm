package 可视化算法.推箱子.求解推箱子的数据结构;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author MT
 * @date 2019-12-02 18:49
 */
public class GameData {

    private int maxTurn;
    private Board starterBoard;
    private int N,M;


    public GameData(String fileName) {

        if (fileName == null) {
            throw new IllegalArgumentException("文件不能为空");
        }

        Scanner scanner = null;
        try {

            File file = new File(fileName);
            if (!file.exists()) {
                throw new IllegalArgumentException("文件不存在");
            }

            FileInputStream fis = new FileInputStream(file);
            scanner = new Scanner(new BufferedInputStream(fis));
            String turnline = scanner.nextLine();
            this.maxTurn = Integer.parseInt(turnline);

            ArrayList<String> lines = new ArrayList<>();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lines.add(line);
            }

            starterBoard = new Board(lines.toArray(new String[lines.size()]));

            this.N = starterBoard.N();
            this.M = starterBoard.M();

            starterBoard.print();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
